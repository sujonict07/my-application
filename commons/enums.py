INACTIVE = 0
ACTIVE = 1

STATUS_TYPE = (
    (ACTIVE, 'Active'),
    (INACTIVE, 'Inactive'),
)

DEMO = 'demo'
PRODUCTION = 'prod'
TESTING = 'test'

INSTANCE_TYPE = (
    (DEMO, 'Demo'),
    (PRODUCTION, 'Prod'),
    (TESTING, 'Test'),
)


INFOBIP = 'infobip'
TWILIO = 'twilio'

SMS_GATEWAY_OPTION = (
    (INFOBIP, 'Infobip'),
    (TWILIO, 'Twilio')
)
