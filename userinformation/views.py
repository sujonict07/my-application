from rest_framework.response import Response
from rest_framework import status

from commons.views import ListCreateApiCustomView
from commons.utils import (GET_BY_UUID)

from commons.utils import get_logger


from .models import (
   UserInfo
)
from .serializers import (
    UserInfoBaseSerializer
)


logger = get_logger()


class UserInfoView(ListCreateApiCustomView):

    # pagination_class = None

    def get_queryset(self):
        queryset = UserInfo.objects.filter(is_deleted=False)
        return queryset

    def create(self, request, *args, **kwargs):

        serializer = UserInfoBaseSerializer(
            data=request.data, context={"request": request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None, **kwargs):
        try:
            user_info = UserInfo.objects.get(uuid=kwargs.get('uuid'))
            serializer = UserInfoBaseSerializer(user_info)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except UserInfo.DoesNotExist:
            return Response({'error': 'User Does not exist'}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        user_info = UserInfo.objects.get(is_deleted=False, uuid=kwargs.get('uuid'))
        serializers = UserInfoBaseSerializer(
            instance=user_info, data=request.data, context={"request": request})
        if serializers.is_valid():
            serializers.save()
            return Response(serializers.data, status=status.HTTP_200_OK)
        return Response({'status': 'failed', 'message': serializers.errors}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        try:
            admin = UserInfo.objects.get(
                is_deleted=False, uuid=kwargs.get(GET_BY_UUID))
        except UserInfo.DoesNotExist:
            return Response({"uuid": kwargs.get(GET_BY_UUID), "status": status.HTTP_404_NOT_FOUND},
                            status=status.HTTP_404_NOT_FOUND)
        admin.is_deleted = True
        admin.save()
        return Response({"uuid": kwargs.get(GET_BY_UUID), "status": status.HTTP_204_NO_CONTENT},
                        status=status.HTTP_204_NO_CONTENT)

    def get_serializer_class(self):
        return UserInfoBaseSerializer
