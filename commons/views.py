from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def get_paginated_response(self, data):
        return Response({
            'paginator': {
                'total_count': self.page.paginator.count,
                'page_size': self.page.paginator.per_page,
                'total_pages': self.page.paginator.num_pages,
                'current_page': self.page.number,
            },
            'results': data
        })


class StandardPaginationForDepartmentAndPosition(StandardResultsSetPagination):
    page_size = 500


class ListCreateApiCustomView(viewsets.ModelViewSet):
    pagination_class = StandardResultsSetPagination


class ListCreateApiForDepartmentAndPositionView(viewsets.ModelViewSet):
    pagination_class = StandardPaginationForDepartmentAndPosition



