import uuid

from django.db import models

from .enums import STATUS_TYPE, ACTIVE

# Create your models here.


class BaseModel(models.Model):
    """
    BaseModel class used for grouping common attributes that will be shared by all other models.
    """
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=False, auto_created=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    status = models.IntegerField(choices=STATUS_TYPE, default=ACTIVE)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True


class CommonModel(BaseModel):
    """
    CommonModel class used for grouping common attributes that will be shared by all other models.
    """
    name = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.name)
