from rest_framework import serializers

from .models import UserInfo


class UserInfoBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserInfo
        fields = (
            'id',
            'uuid',
            'name',
            'email',
            'phone',
            'address',
            'avatar',
        )


class UserInfoListSerializer(UserInfoBaseSerializer):
    class Meta:
        model = UserInfo
        fields = UserInfoBaseSerializer.Meta.fields + ('is_deleted',)
        read_only_fields = ('id', 'uuid',)

