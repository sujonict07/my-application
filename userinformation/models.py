from django.db import models
from django.core.validators import URLValidator
from commons.models import CommonModel


class UserInfo(CommonModel):
    phone = models.CharField(max_length=17, blank=True)
    email = models.EmailField(max_length=70, blank=True, null=True)
    address = models.TextField(null=True, blank=True)
    avatar = models.TextField(validators=[URLValidator()], null=True,
                              default='http://kutumbitastorage.s3.amazonaws.com/avatars/FUFC6BP8NH.jpeg')

    class Meta:
        db_table = 'administrators'

    def __str__(self):
        return str(self.name)
