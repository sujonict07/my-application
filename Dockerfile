FROM python:3.6

# root directory for our project in the container

RUN mkdir /myapplication

# working directory
WORKDIR /myapplication

# copy requirements.txt file
COPY ./requirements.txt ./

RUN pip install -r requirements.txt

# copy the current directory contents into the container
ADD myappliction /myapplication/
