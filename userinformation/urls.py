from django.urls import path

from commons.utils import support_method, retrieve_method

from .views import (
 UserInfoView
)

urlpatterns = [
    path('users', UserInfoView.as_view(support_method), name='userinfo-list'),
    path('users/<uuid>', UserInfoView.as_view(retrieve_method), name='userinfo-detail'),
]
